/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bbdd;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author elcer
 */
public class ConexionBBDD {
    private static Connection conexion;
    private boolean activa = false;
    
    public ConexionBBDD() {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            String cx = "jdbc:mysql://localhost:3306/generico";
            conexion = DriverManager.getConnection(cx,"admin","root");
            activa = true;
        } catch (ClassNotFoundException | SQLException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(ConexionBBDD.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    public void cerrarConexion() {
        try {
            if (activa) {
                conexion.close();
                activa = false;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConexionBBDD.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    public boolean estaActiva() {
        return activa;
    }
    
    public int insertBBDD(String ssql) {  //ssql = sentencia de mysql (insert, select, etc)
        int res = 0;
        try {
            if (conexion != null) {
                Statement cst = conexion.createStatement();
                res = cst.executeUpdate(ssql); 
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConexionBBDD.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }
    
    public ResultSet selectBBDD(String ssql) {
        ResultSet res = null;
        try {
            if (conexion != null) {
                Statement cst = conexion.createStatement();
                res = cst.executeQuery(ssql);            
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConexionBBDD.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }
    
    public void commit() {
        try {
            conexion.commit();
        } catch (SQLException ex) {
            Logger.getLogger(ConexionBBDD.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void rollback() {
        try {
            conexion.rollback();
        } catch (SQLException ex) {
            Logger.getLogger(ConexionBBDD.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
