/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;
import bbdd.ConexionBBDD;

/**
 *
 * @author elcer
 */
public class Empleado implements java.io.Serializable {
    
    private int numPagas;
    private boolean contratoTemporal = false;
    private String nombre, apellidos, departamento;
    private double salario;
        
    public Empleado() {
        super();
    }
    
    public void setDatosEmpleado(String nombre, String apellidos, 
            String departamento, double salario, int numPagas) {
        this.nombre = nombre; this.apellidos = apellidos;
        this.salario = salario; 
        this.numPagas = numPagas;
        this.departamento = departamento;
    }
    
    public void contratoTemporal() {
        contratoTemporal = true;
    }
    
    public String getDepartamento() {
        return departamento;
    }
    
    public String getNombreCompleto() {
        return apellidos + ", " + nombre;
    }
    
    public double getSalarioNeto() {
        double salarioNetoAnual;
        if (contratoTemporal) salarioNetoAnual = salario*(1-0.064); 
        else  salarioNetoAnual = salario*(1-0.0635);
        
        salarioNetoAnual = salarioNetoAnual*(1-0.16);
        double salarioNeto = salarioNetoAnual/numPagas;
        
        return Math.floor(salarioNeto);
    }
        
    public boolean saveBBDD() {
        ConexionBBDD conexion = new ConexionBBDD();
        
        String tipoContrato;
        if (contratoTemporal) tipoContrato = "Temporal";
        else tipoContrato = "Indefinido";
        
        String ssql = "INSERT INTO Empleados VALUES ('";
        ssql += nombre + "','" + apellidos + "'," + salario;
        ssql += "," + numPagas + ",'" + tipoContrato + "')";
        int res = conexion.insertBBDD(ssql);
        conexion.cerrarConexion();
        return (res != 0);
    }
    
    @Override
    public String toString() {
        return "";
    }
}
