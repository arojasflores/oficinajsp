
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Oficina</title>
    </head>
    <body>
        <jsp:useBean id="empleado" scope="request" class="beans.Empleado"></jsp:useBean>
        <%
            String nombre = null, apellidos = null, departamento = null, contrato;
            double salario = 0;
            int numPagas = 12;
            
            if (request.getParameter("send") != null) {
                nombre = request.getParameter("nombre");
                apellidos = request.getParameter("apellido1") + 
                        " " + request.getParameter("apellido2");
                departamento = request.getParameter("departamento");
                
                salario = Double.parseDouble(request.getParameter("salario"));
                if (request.getParameter("numPagas") != null) {
                    numPagas = Integer.parseInt(request.getParameter("numPagas"));
                } 

                if (nombre != null && apellidos != null) {
                    contrato = request.getParameter("contrato");
                    empleado.setDatosEmpleado(nombre, apellidos, departamento, 
                                                    salario, numPagas);
                    if (contrato.equals("temporal")) empleado.contratoTemporal();
                    empleado.saveBBDD();
                }
                
                RequestDispatcher dispatcher;
                String redirectJSP = "salarioNeto.jsp";
                String salarioStr = String.valueOf(empleado.getSalarioNeto());
                
                request.setAttribute("nombreEmp", empleado.getNombreCompleto());
                request.setAttribute("depEmp", empleado.getDepartamento());
                request.setAttribute("salarioEmp", salarioStr);

                dispatcher = request.getRequestDispatcher(redirectJSP);
                dispatcher.forward(request, response);
                
            }
        %>
        
            <form action="index.jsp" method="post">
                <table>
                    <tr>
                        <td>Introducir Nombre:</td>
                        <td>
                            <input type="text" name="nombre">
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Introducir Apellidos:</td>
                        <td>
                            <input type="text" name="apellido1">
                        </td>
                        <td>
                            <input type="text" name="apellido2">
                        </td>
                    </tr>
                    <tr>
                        <td>Departamento:</td>
                        <td>
                            <input type="text" name="departamento">
                        </td>
                        <td></td>
                    </tr>
                </table>
                <br/>
                
                Tipo de Contrato:
                <select id="contrato" name="contrato">
                    <option value="indefinido">Indefinido</option>
                    <option value="temporal">Temporal/Becario</option>
                </select>
                <br/>
                <table>
                    <tr>
                        <td>Introducir Salario y N&uacute;mero de pagas mensuales:</td>  
                        <td></td>
                    </tr>
                    <tr>
                        <td><input type="text" name="salario"></td>
                        <td><input type="text" name="numpagas"></td>
                    </tr>
                </table>
                <input type="submit" name="send" value="Confirmar">
            </form>
    </body>
</html>
