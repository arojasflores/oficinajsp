<%-- 
    Document   : salarioNeto
    Created on : 01-feb-2017, 17:09:28
    Author     : elcer
--%>

<%@page import="beans.Empleado"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Oficina</title>
    </head>
    <body>
        <jsp:useBean id="empleado" scope="request" class="beans.Empleado"></jsp:useBean>
        <table>
            <tr>
                <td>
                    <% 
                        String nombEmpleado = (String) request.getAttribute("nombreEmp");
                        out.println(nombEmpleado);
                    %>
                </td>     
                <td>, DEPARTAMENTO DE 
                    <% 
                        String depEmpleado = (String) request.getAttribute("depEmp");
                        out.println(depEmpleado);
                    %>
                </td>
            </tr>
            <tr>
                <td>SALARIO NETO MENSUAL:</td>
                <td><% 
                        String salarioStr = (String) request.getAttribute("salarioEmp"); 
                        double salarioEmpleado = Double.parseDouble(salarioStr);
                        out.println(salarioEmpleado);
                    %> &euro;
                </td>
            </tr>
        </table>
        <a href="index.jsp">Volver Atr&aacute;s</a>
    </body>
</html>
